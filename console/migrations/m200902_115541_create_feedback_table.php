<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback}}`.
 */
class m200902_115541_create_feedback_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%feedback}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'surname' => $this->string(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string(),
            'body' => $this->text()->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%feedback}}');
    }
}
