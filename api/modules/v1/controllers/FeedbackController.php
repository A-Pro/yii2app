<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;

/**
 * Feedback Controller API
 */
class FeedbackController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Feedback';
}


