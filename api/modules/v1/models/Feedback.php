<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
/**
 * Feedback Model
 */
class Feedback extends ActiveRecord
{
	public static function tableName()
	{
		return 'feedback';
	}

    public function rules()
    {
        return [
            [['name', 'email', 'body'], 'required', 'message' => 'Поле обязательно для заполнения'],
            [['name', 'surname'], 'trim'],
            [['name', 'surname'], 'string', 'min'=>3, 'message' => 'Поле должно быть длинее 3 символов'],
            [['name', 'surname'], 'match', 'pattern'=>'/^[а-яА-ЯёЁa-zA-Z]+$/u', 'message' => 'Поле содержит небуквенные значения'],
            [['name', 'surname'], 'filter', 'filter'=>function($value){
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            [['phone', 'body'], 'safe'],
            ['email', 'email', 'message' => 'Email не корректен']
        ];
    }
}
