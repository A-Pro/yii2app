<?php
namespace frontend\models;

use yii\base\Model;
use common\models\Feedback;

class EntryForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $phone;
    public $body;
    public $verifyCode;

    public function rules()
    {
        return [
            [['name', 'email', 'body'], 'required', 'message' => 'Поле обязательно для заполнения'],
            [['name', 'surname'], 'trim'],
            [['name', 'surname'], 'string', 'min'=>3, 'message' => 'Поле должно быть длинее 3 символов'],
            [['name', 'surname'], 'match', 'pattern'=>'/^[а-яА-ЯёЁa-zA-Z]+$/u', 'message' => 'Поле содержит небуквенные значения'],
            [['name', 'surname'], 'filter', 'filter'=>function($value){
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            ['phone', 'safe'],
            ['email', 'email', 'message' => 'Email не корректен'],
            ['body', 'string', 'min'=>100, 'message' => 'Текст должен содержать более 100 символов'],
            ['verifyCode', 'captcha', 'message' => 'Неверно указано кодовое слово'],
        ];
    }

    public function addFeedback()
    {
        if (!$this->validate()) {
            return null;
        }

        $feedback = new Feedback();
        $feedback->name = $this->name;
        $feedback->surname = $this->surname;
        $feedback->email = $this->email;
        $feedback->phone = $this->phone;
        $feedback->body = $this->body;
        return $feedback->save();
    }

}