<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="single">
<?php $this->beginBody() ?>
    <header class="single-header">
        <nav class="main-menu">
            <ul class="main-menu__items">
                <li class="main-menu__item">
                    <a class="main-menu__item-link" href="<?php echo Yii::$app->homeUrl?>">Главная</a>
                </li>
                <li class="main-menu__item">
                    <a class="main-menu__item-link main-menu__item-link_current" href="<?=Yii::$app->urlManager->createUrl(['markup/page'])?>">Типовая</a>
                </li>
            </ul>
        </nav>
    </header>
    <main class="single-content">
        <div class="single-content__wrapper">
            <div class="left-navigation">
                <div class="left-navigation__item">
                    <a href="#" class="left-navigation__item-link">Файловый сервер</a>
                </div>
                <div class="left-navigation__item">
                    <a href="#" class="left-navigation__item-link">Сервер музыки</a>
                </div>
                <div class="left-navigation__item">
                    <a href="#" class="left-navigation__item-link">Игровой сервер</a>
                </div>
            </div>
            <div class="right-content">
                <div class="news-block">
                    <div class="news-block__title">Новости</div>
                    <div class="news-block__items">
                        <div class="news-block__item">
                            <div class="news-block__item-pic"><img src="https://cutewallpaper.org/21/4k-fractal-wallpaper/71-Fractal-Wallpapers-on-WallpaperPlay.jpg" alt=""></div>
                            <div class="news-block__item-title">Теперь наше покрытие намного больше</div>
                            <p class="news-block__item-text">
                                Равным образом консультация с широким активом требуют определения и уточнения модели развития. 
                                Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет 
                                оценить значение новых предложений.
                            </p>
                            <a href="#" class="news-block__item-more">Подробнее</a>
                        </div>
                        <div class="news-block__item">
                            <div class="news-block__item-pic"><img src="https://bluetooth.ru.com/srcin/provision-bt.jpg" alt=""></div>
                            <div class="news-block__item-title">Теперь наше покрытие намного больше</div>
                            <p class="news-block__item-text">
                                Равным образом консультация с широким активом требуют определения и уточнения модели развития.
                                Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет
                                оценить значение новых предложений.
                            </p>
                            <a href="#" class="news-block__item-more">Подробнее</a>
                        </div>
                        <div class="news-block__item">
                            <div class="news-block__item-pic"><img src="https://img3.goodfon.ru/wallpaper/middle/e/41/krugi-svet-yadro-vzryv.jpg" alt=""></div>
                            <div class="news-block__item-title">Теперь наше покрытие намного больше</div>
                            <p class="news-block__item-text">
                                Равным образом консультация с широким активом требуют определения и уточнения модели развития.
                                Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности позволяет
                                оценить значение новых предложений.
                            </p>
                            <a href="#" class="news-block__item-more">Подробнее</a>
                        </div>
                    </div>
                    <div class="list__pagination">
                        <span class="list__pagination-title">Страницы</span>
                        <ul class="list__pagination-items">
                            <li class="list__pagination-item"><span>1</span></li>
                            <li class="list__pagination-item"><a href="#">2</a></li>
                            <li class="list__pagination-item"><a href="#">3</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="single-footer">
        <nav class="main-menu">
            <ul class="main-menu__items">
                <li class="main-menu__item">
                    <a class="main-menu__item-link" href="<?php echo Yii::$app->homeUrl?>">Главная</a>
                </li>
                <li class="main-menu__item">
                    <a class="main-menu__item-link main-menu__item-link_current" href="<?=Yii::$app->urlManager->createUrl(['markup/page'])?>">Типовая</a>
                </li>
            </ul>
        </nav>
        <div class="single-footer__copyright">&copy; Copyright, 2020</div>
    </footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
