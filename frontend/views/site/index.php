<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'My Contact Form';
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'feedback']); ?>

            <?= $form->field($model, 'name')->label('Имя') ?>

            <?= $form->field($model, 'surname')->label('Фамилия') ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'phone')->label('Телефон')->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7(999)999-99-99',
            ]) ?>

            <?= $form->field($model, 'body')->textarea(['rows' => 6])->label('Сообщение') ?>

            <?= $form->field($model, 'verifyCode')->label('Код проверки')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
