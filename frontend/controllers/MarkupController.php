<?php
namespace frontend\controllers;

use yii\web\Controller;

/**
 * Site controller
 */
class MarkupController extends Controller
{
    /**
     * Render new layout.
     */
    public $layout = 'single';
    /**
     * Displays single page.
     */
    public function actionPage()
    {
        return $this->render('page');
    }
}
