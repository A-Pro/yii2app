<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Feedback;

class FeedbackSearch extends Feedback
{
    public function rules()
    {
        //use fields for filter
        return [
            [['id'], 'integer'],
            [['name', 'email', 'surname', 'phone', 'body'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Feedback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query  ->andFilterWhere(['id' => $this->id])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'surname', $this->surname])
                ->andFilterWhere(['like', 'phone', $this->phone])
                ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }

    public function searchByUser($params, $userEmail)
    {
        $query = Feedback::find()->where(['email'=>$userEmail]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query  ->andFilterWhere(['id' => $this->id])
                ->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'surname', $this->surname])
                ->andFilterWhere(['like', 'phone', $this->phone])
                ->andFilterWhere(['like', 'body', $this->body]);

        return $dataProvider;
    }
}