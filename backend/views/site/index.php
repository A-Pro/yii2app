<?php

/* @var $this yii\web\View */

use yii\grid\GridView;

$this->title = 'Feedback list';

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h1>Результаты формы</h1>
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <a href="<?=Yii::$app->urlManager->createUrl(['site/userdata'])?>">Результаты форм пользователя</a>
            </div>
        </div>
    </div>
</div>
