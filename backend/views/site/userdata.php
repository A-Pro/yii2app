<?php

/* @var $this yii\web\View */

use yii\grid\GridView;

$this->title = 'Feedback list';

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <h1>Результаты формы пользователя <?php echo $userData->username?></h1>
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <a href="<?php echo Yii::$app->homeUrl?>">Все результаты форм</a>
            </div>
        </div>
    </div>
</div>
